import React from 'react';
import PropTypes from 'prop-types';

import Editable from 'editable';

import './html.scss';

/**
 * Html
 * @description [description]
 * @example
  <div id="Html"></div>
  <script>
	ReactDOM.render(React.createElement(Components.Html, {
		content			: `Example x`,
		className		: 'foo'
	}), document.getElementById("Html"));
  </script>
 */
const Html = props => {
	return <Editable {...props} baseClass="html" />;
};

Html.defaultProps = {
	...Editable.defaultProps,
	...{
		tag: 'div'
	}
};

Html.propTypes = {
	...Editable.propTypes,
	...{}
};

export default Html;
